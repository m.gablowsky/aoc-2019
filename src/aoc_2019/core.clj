(ns aoc-2019.core
  (:require [clojure.string :as s]))

(defonce resource-base "resources/input-day")

(defn read-single-line-input [day]
  (s/replace (slurp (str resource-base day)) "\n" ""))

(defn read-multi-line-input [day]
  (s/split (slurp (str resource-base day)) #"\n"))
