(ns aoc-2019.day1
  (:require [aoc-2019.core :refer :all]))

;; https://adventofcode.com/2019/day/1

(defn calc-fuel [m]
  (- (Math/floor (/ m 3)) 2))

(defn day1-1 []
  (reduce + (map #(calc-fuel (Integer/parseInt %)) (read-multi-line-input 1))))

(defn calc-fuel-recur [m]
  (let [fuel (calc-fuel m)]
    (if (<= fuel 0)
      0
      (+ fuel (calc-fuel-recur fuel)))))

(defn day1-2 []
  (reduce + (map #(calc-fuel-recur (Integer/parseInt %)) (read-multi-line-input 1))))
