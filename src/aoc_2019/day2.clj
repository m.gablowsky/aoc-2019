(ns aoc-2019.day2
  (:require [aoc-2019.core :refer :all]
            [clojure.string :as s]))

;; https://adventofcode.com/2019/day/2

(defn- create-memory-state [input-string]
  (into [] (map #(Integer/parseInt %) (s/split input-string #","))))

(defn- init-machine [memory-state]
  {:memory memory-state
   :inst-pointer 0
   :halted false})

(defonce opcode-map {1 + 2 *})
(defonce raw-input (read-single-line-input 2))

(defn- fetch-opcode[{:keys [memory inst-pointer]}]
  (nth memory inst-pointer))

(defn- val-at [memory idx]
  (nth memory idx))

(defn- param-val [memory inst-pointer param-id]
  (val-at memory (val-at memory (+ inst-pointer param-id))))

(defn- do-op [memory inst-pointer op]
  (assoc memory (val-at memory (+ inst-pointer 3)) (apply op [(param-val memory inst-pointer 1) (param-val memory inst-pointer 2)])))

(defn- execute-instruction [{:keys [memory inst-pointer] :as machine} opcode]
  (let [operation (opcode-map opcode)]
    (-> machine
        (assoc :memory (do-op memory inst-pointer operation))
        (assoc :inst-pointer (+ inst-pointer 4)))))

(defn- run-machine [{:keys [halted] :as machine}]
  (if (false? halted)
    (let [opcode (fetch-opcode machine)]
      (if (= opcode 99)
        (assoc machine :halted true)
        (recur (execute-instruction machine opcode))))))

(defn- run-with-params [[noun verb]]
  (->
   (create-memory-state raw-input)
   (assoc 1 noun)
   (assoc 2 verb)
   (init-machine)
   (run-machine)))

(defn day2-1 []
  (first (:memory (run-with-params [12 2]))))

(defn- calculate-result [[noun verb]]
  (+ (* 100 noun) verb))

(defn- next-inputs [[noun verb]]
  (if (< verb 99)
    [noun (inc verb)]
    [(inc noun) 0]))

(defn day2-2 []
  (loop [inputs [0 0]]
    (if (= 19690720 (first (:memory (run-with-params inputs))))
      (calculate-result inputs)
      (recur (next-inputs inputs)))))
