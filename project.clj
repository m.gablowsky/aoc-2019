(defproject aoc-2019 "0.1.0-SNAPSHOT"
  :description "Advent of code 2019"
  :url "http://example.com/FIXME"
  :license {:name "GPL 3"
            :url "see LICENSE file"}
  :dependencies [[org.clojure/clojure "1.9.0"]]
  :repl-options {:init-ns aoc-2019.core})
